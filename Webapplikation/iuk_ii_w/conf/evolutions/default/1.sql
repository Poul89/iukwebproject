# Book schema

# --- !Ups

create table book (
  id                            integer not null,
  name                         varchar(255),
  isbn13                        varchar(255),
  isbn10                           varchar(255),
  description                       varchar(255),
  publisher                         varchar(255),
  price                         integer,
  pages                            integer(900),
  constraint pk_book primary key (id)
);
create sequence book_seq;


# --- !Downs

drop table if exists book;
drop sequence if exists book_seq;

