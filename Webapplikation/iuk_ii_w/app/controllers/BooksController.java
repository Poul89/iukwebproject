package controllers;

import play.mvc.*;
import play.libs.concurrent.HttpExecutionContext;

import java.util.ArrayList;
import java.util.List;

import services.DefaultBookService;
import services.BookService;
import views.html.*;

import java.util.stream.Collectors;
import java.util.concurrent.CompletionStage;

import repository.BookRepository;
import play.data.Form;
import play.data.FormFactory;

import static play.libs.Json.toJson;
import static play.libs.Json.*;
import javax.inject.Inject;

public class BooksController extends Controller {

/*Status cods
    https://www.playframework.com/documentation/1.2.x/api/play/mvc/Http.StatusCode.html
    https://www.playframework.com/documentation/1.1/api/play/mvc/Controller.html
    */

    int ce = 444;
    String ha = "Hans";
    private final FormFactory formFactory;
    private final BookRepository bookRepository;
    private final BookService bookService;
    private final HttpExecutionContext ec;
    private final DefaultBookService dbs;

    @Inject
    public BooksController(BookService bookService) {
        // this.formFactory = formFactory;
        // this.personRepository = personRepository;
        // this.ec = ec;
        this.bookService = bookService;
    }

// book service without DB

    /*
        public Result getAllBooks(String BookName) {
            if (BookName != null) {
                //book = DBS.get(BookName);
               // JsonNode resp = Json.toJson(DBS.get());
             // return ok(index.render(book));
                return ;
               // return ok(getBookSearch.render(BookName));
              /*  for (Book bo : bo)
                    if (book.getName.equals(BookName)) {
                        return bo;
                    }

            } else {
              //  return ok(getAllBooks.render());
                // return book;
               // return ok(index.render(books));
                return ;
            }
        }

        public Result postBookDetail(int id) {
            if (id <= 0) {
                return notFound();
            } else {
                return ok(postBookDetail.render());
            }
        }

        public Result putBookDetailForBookWithID(int id) {

            return ok(putBookDetailForBookWithID.render());
        }

        public Result getBookDetailWithBookID(int id) {

            return ok(getBookDetailWithBookID.render(id));
        }

        public Result getBookSearch() {

            return ok(views.html.getBookSearch.render());
        }

        public Result deleteBookWithID(int id) {

            return ok(deleteBookWithID.render());
        }
    */
    // tutorial:
   /* public Result index(Http.Request request) {

        return ok(index.render(request));
    }
*/
    public CompletionStage<Result> books(String q) {
        return bookService.get().thenApplyAsync(bookStream -> ok(Json.toJson(bookStream.collect(Collectors.toList()))));
    }

    public CompletionStage<Result> create(final Http.Request request) {
        final JsonNode json = request.body().asJson();
        Book book = formFactory.form(Book.class).bindFromRequest(request).get();
       /* return bookRepository
                .add(book)
                .thenApplyAsync(p -> redirect(routes.BookController.index()), ec.current());
                */
        return bookService.update(bookToUpdate).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    public Result save(final Http.Request request) {
        Book book = formFactory.form(Book.class).bindFromRequest(request).get();
        if (request == 400) {
            flash("danger", "Please Correct the Form Below");
            return badRequest(create.render(request));
        }

        book.save();
        flash("success", "Book Save Successfully");
        return redirect(routes.BooksController.index());
    }

    public Result edit(Integer id) {

        Book book = Book.find.byId(id);
        if (book == null) {
            return notFound(_404.render());
        }
        Form<Book> book = formFactory.form(Book.class).fill(book);
        return ok(edit.render(book));
    }

    public CompletionStage<Result> details(long id) {
        return bookService.get(id).thenApplyAsync(book -> ok(Json.toJson(book)));
    }

    public CompletionStage<Result> update(long id, Http.Request request) {
        final JsonNode json = request.body().asJson();
        final Book bookToUpdate = Json.formJson(json, Book.class);
        Form<Book> book = formFactory.form(Book.class).bindFromRequest();
        bookToUpdate.setId(id);
        return bookService.update(bookToUpdate).thenApplyAsync(book -> ok(Json.toJson(book)));
       /*   Model and FaCTORY IMPLEMENTIERUNG
        if (book.hasErrors()){
            flash("danger","Please Correct the Form Below");
            return badRequest();
        }

        Book book = bookForm.get();
        Book oldBook = Book.find.byId(book.id);
        if(oldBook ==null){
            flash("danger","Book Not Found");
            return notFound();
        }
        oldBook.name = book.name;
        oldBook.description = book.description;
        oldBook.price = book.price;
        oldBook.isbn10 = book.isbn10;
        oldBook.isbn13 = book.isbn13;
        oldBook.pages = book.pages;
        oldBook.publisher = book.publisher;
        oldBook.update();
        flash("success","Book Updated Successfully");
        return ok();
        */
    }

    public CompletionStage<Result> delete(long id) {
        return bookService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }

    public Result destroy(Integer id) {

        Book book = Book.find.byId(id);
        if (book == null) {
            flash("danger", "Book Not Found");
            return notFound();
        }
        book.delete();
        flash("success", "Book Deleted Successfully");

        return ok();
    }

    public Result show(Integer id) {
        Book book = Book.find.byId(id);
        if (book == null) {
            return notFound(_404.render());
        }
        return ok(show.render(book));
    }
}

