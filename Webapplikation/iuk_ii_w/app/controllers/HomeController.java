package controllers;

import play.mvc.*;

import java.util.ArrayList;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

/*Status cods
    https://www.playframework.com/documentation/1.2.x/api/play/mvc/Http.StatusCode.html
    https://www.playframework.com/documentation/1.1/api/play/mvc/Controller.html
    */

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    int ce = 444;
    String ha = "Hans";


    public Result index() {

        return ok(views.html.index.render());
    }

    public Result rp(String name, String lastName) {
       // return ok(views.html.rp.render(name, lastName));
        return ok("Hello "+name+ " "+ lastName);
    }


}

