package models;

import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;


@Entity(name = "book")
public class Book {
    @Id
    @Constraints.Required
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Constraints.MaxLength(255)
    @Constraints.MinLength(5)
    @Constraints.Required
    private String name;
    @Constraints.MaxLength(255)
    @Constraints.MinLength(10)
    @Constraints.Required
    private String isbn13;
    @Constraints.MaxLength(255)
    @Constraints.MinLength(10)
    @Constraints.Required
    private String isbn10;
    @Constraints.MaxLength(255)
    @Constraints.MinLength(20)
    @Constraints.Required
    private String description;
    @Constraints.MaxLength(255)
    @Constraints.MinLength(10)
    @Constraints.Required
    private String publisher;
    @Constraints.Max(900)
    @Constraints.Min(1)
    @Constraints.Required
    private Integer pages;

    private Integer price;

    /*
    public Book(Integer id, String name, String isbn13 String isbn10, String description, String publisher, Integer pages){
    this.id = id;
    this.name = name
    this.isbn10 = isbn10;
    this.description = description;
    this.publisher = publisher;
    this.pages = pages;
    }
    */
    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public int getPages() {
        return pages;
    }

    public String getDescription() {
        return description;
    }

    public String getIsbn10() {
        return isbn10;
    }

    public String getIsbn13() {
        return isbn13;
    }

    public String getName() {
        return name;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIsbn10(String isbn10) {
        this.isbn10 = isbn10;
    }

    public void setIsbn13(String isbn13) {
        this.isbn13 = isbn13;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }
    // public static Finder<Integer,Book> find = new Finder<>(Book.class);
}
