package services;
import com.fasterxml.jackson.databind.JsonNode;
import com.typesafe.config.Config;
import javax.inject.Inject;
import java.util.List;
import models.Book;
import play.libs.Json;
import play.libs.ws.*;
import models.nytimes.NYTimesBestseller;
import models.nytimes.NYTimesBook;
import models.nytimes.NYTimesReviewList;
import com.typesafe.config.Config;
import java.util.concurrent.CompletionStage;

public class DefaultNYTimesService implements NYTimesService,WSBodyReadables{

    private final WSClient ws;
    private final String apiKey;

    @Inject
    public DefaultNYTimesService(WSClient ws,Config config){
        this.ws=ws;
        this.apiKey=conifg.getString("nytimes.api.key");

    }
    @Override
    public CompletionStage<List<Book>>bestseller(){
        final String url="https://api.nytimes.com/svc/books/v3/list/overview.json";
        final WSRequest request=ws.url(url.addQueryParameter(api-key,apiKey));
        final ComplationStage<JsonNode> jsonPromise=request.get().thenApply(result->result.getBody(json));
        final ComplationStage<NyTimesBestseller> bestsellerPromise=jsonPromise.thenApplyAsync(json->Json.formJson(json,NYTimesBestseller.class));
        final ComplationStage<List<Book>>bookPromise=bestsellerPromise.thenApplyAsync(bestseller->map(bestseller));
        return bookPromise;

    }
    public List<Book> map(NYTimesBestseller bestseller){
        final List<Book> books=new ArrayList<>();
        for(NYTimesBestseller list:bestseller.getResult().getLists()){
            if(list.getList_id()==704){
                list.getBook().forEach(myTimesBook->books.add(map(nyTimesBook)));
            }
        }
        return books;

    }
    private Book map(NYTimesBook myTimesBook){
        final Book book = new Book();
        book.setDescription(myTimesBook.getDescription());
        book.setIsbn10(myTimesBook.getPrimary_isbn10());
        book.setIsbn13(myTimesBook.getPrimary_isbn13());
        book.setTitle(myTimesBook.getTitle());
        book.setPublisher(myTimesBook.getPublisher());
        return book;
    }
}