package services;

import com.google.inject.ImplementedBy;
import models.Book;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

@ImplementedBy(DefaultBookService.class)
public interface BookService{

        CompletionStage<Stream<Book>>get();
        CompletionStage<Book>get(final long id);
        CompletionStage<Book>delete(final long id);
        CompletionStage<Book>update(final Book updatedBook);
        CompletionStage<Book> add(final Book book);
        Book getDummy();
        }