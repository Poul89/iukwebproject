package services;

import repository.BookRepository;

import java.util.List;

import models.Book;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class DefaultBookService implements BookService {
    /**
     * Return's list of all books.
     *
     * @return list of all books
     */
    private BookRepository bookRepository;
  /*
   private  List<Book> books;


  public  List<Book> get() {
      return books;
  };
    /**
     * Returns book with given identifier.
     * @param id book identifier
     * @return book with given identifier or {@code null}

   public Book get(final int id){

       for (Book i: books){
           if(i.getId() == id) {
               return i;
           }
       }
       return null;
   };
    /**
     * Removes book with given identifier.
     * @param id book identifier
     * @return {@code true} on success {@code false} on failure

   public  boolean delete(final int id){

       for (Book i: books){
           if(i.getId() == id) {
              books.remove(i);
              return true;
           }
       }
       return false;
   };
    /*
    *
     * Updates book with given identifier.
     * @param updatedBook book with updated fields
     * @return updated book

   public Book update(final Book updatedBook){
       for (Book i: books){
           if(i.getId() == updatedBook.getId()) {
               books.remove(i);
               books.add(updatedBook);
           }
       }
       return updatedBook;
   };
    /**
     * Adds the given book.
     * @param book to add
     * @return added book

    public Book add(final Book book){
         Book newBook;
         books.add(book);
         newBook = books.get(books.indexOf(book));
         return newBook;
    };
    */


    @Inject
    public DefaultBookService(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public Book getDummy() {
        final Book book = new Book();
        book.setId(1l);
        book.setDescription("Dieses Lehrbuch bietet eine umfassende Einführung in Grundlagen und Methoden der Computerlinguistik und stellt die wichtigsten Anwendungsgebiete in der Sprachtechnologie vor. Es richtet sich gleichermaßen an Studierende der Computerlinguistik und verwandter Fächer mit Bezug zur Verarbeitung natürlicher Sprache wie an Entwickler sprachverarbeitender Systeme.");
        book.setIsbn10("3827420237");
        book.setIsbn13("978-3827420237");
        book.setPages(736);
        book.setPublisher("Spektrum Akademischer Verlag");
        book.setTitle("Computerlinguistik und Sprachtechnologie: Eine Einführung (German Edition)");
        return book;
    }

    public CompletionStage<Stream<Book>> get() {
        return bookRepository.list();
    }

    public CompletionStage<Book> get(final Long id) {
        return bookRepository.find(id);
    }

    public CompletionStage<Boolean> delete(final long id) {
        return bookRepository.remove(id);
    }

    public CompletionStage<Book> update(final Book book) {
        return bookRepository.update(book);
    }

    public CompletionStage<Book> add(final Book book) {
        return bookRepository.add(book);
    }
}