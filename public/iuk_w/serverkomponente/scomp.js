const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 3000
app.use(express.static('../public'))

app.use(bodyParser.json());

// ausgabe Hello World bei normaler url anfrage ohne parameter
app.get('/', (req, res) => res.send('Hello World!'))



//ausgabe des Produkts mit der Id welche als parameter in der url mitgegeben wird (get)
app.get('/products', (req,res) => res.send(JSON.stringify(products)));
app.get('/products/:id', function(req,res){
    let idgesucht = req.params.id;
    console.log(idgesucht + 'idgesucht');
    for (var i of products){
        console.log(i.id + 'i.id');
        if(i.id == idgesucht){
            console.log('if');
            res.send(JSON.stringify(i));
        }
    }

});

app.listen(port, () => console.log('Example app listening on port ${port}! '))


let products = [
    {
        id: 1,
        title: 'FHGR Chur Hoodie Navy Athletic',
        description: 'Hoodie in diversen Grössen erhältlich mit FHGR Logo',
        category: 'HOODIES',
        image: 'images/product1.jpg',
        price: {
            value: 59.00,
            currency: 'CHF'
        }
    },
    {
        id: 2,
        title: 'Produkt zwei',
        description: 'Hoodie der scheisse aussieht',
        category: 'Pullover',
        image: 'images/product1.jpg',
        price: {
            value: 100.00,
            currency: '€'
        }
    }

]