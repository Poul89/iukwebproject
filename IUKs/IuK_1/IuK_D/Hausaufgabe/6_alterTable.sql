/*
Alter Table anweisungen
psql -U student -f ""/6_alterTable.sql

Für unterschiedliche Datenbanktypen:>
SQL Server / MS Access:
ALTER TABLE table_name
ALTER COLUMN column_name datatype;

My SQL / Oracle (prior version 10G):
ALTER TABLE table_name
MODIFY COLUMN column_name datatype;

Oracle 10G and later:
ALTER TABLE table_name
MODIFY column_name datatype;
  */

/*Nicht verwendete Anweisungen wurden als Übung verwendet, diese sind auskommentiert damit alles der Reihenfolge nach funktioniert*/

--Basics add, alter and drop colums
ALTER TABLE tAutoren ADD AutorenName4 VARCHAR(50);
ALTER TABLE tAutoren ADD AutorenName5 VARCHAR(50);
ALTER TABLE tBuch ALTER COLUMN Buchtitel TYPE TEXT;
-- ALTER TABLE tBuch ALTER COLUMN Buchtitel VARCHAR(50);
ALTER TABLE tAutoren DROP COLUMN AutorenName5;

--ALTER TABLE tToDelete ADD PRIMARY KEY (Buch2ID);

-- CONSTRAINT PRIMARY KEY
-- ALTER TABLE ** DROP and ADD CONSTRAINT PRIMARY KEY Gehen nach den DEPENDS nicht mehr, diese sind beim erstellen schon festgelegt worden
--mit CASCADE
ALTER TABLE tAutoren DROP CONSTRAINT AutorenID CASCADE;
ALTER TABLE tAutoren ADD CONSTRAINT AutorenKey PRIMARY KEY (AutorenID);
ALTER TABLE tBuch ADD FOREIGN KEY (AutorenID) REFERENCES tAutoren(AutorenID);

--CHECK
ALTER TABLE tAutoren ADD CONSTRAINT firstAutorIsSet CHECK (AutorenName1 != NULL);
--DEFAULT
ALTER TABLE tAutoren ALTER COLUMN AutorenName1 SET DEFAULT 'HANS MEIER';
--UNIQUE
ALTER TABLE tBestellungen ADD BestellungsID2 INT;
-- fügt tautoren_autorenid_seq hinzu ..(SQL | SEQUENCES. Sequence is a set of integers 1, 2, 3, … that are generated and supported by some database systems to produce unique values on demand.
-- A sequence is a user defined schema bound object that generates a sequence of numeric values.)
ALTER TABLE tBestellungen ADD UNIQUE (BestellungsID2);
--IDENTITY
--ALTER TABLE tAutoren ADD AutorenewID INT NOT NULL;
ALTER TABLE tAutoren ALTER COLUMN AutorenID ADD GENERATED ALWAYS AS IDENTITY;

-- DROP TABLE tToDelete;