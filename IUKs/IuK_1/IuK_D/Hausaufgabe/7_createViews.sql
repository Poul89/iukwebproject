/*
Create Views Anweisungen
psql -U student -f ""/7_createViews.sql
  */

-- Bennenung der Views nach Vorgabe und geordnet nach eigenem ermessen. Daher sehen die View Abfragen recht lange aus.

-- Mit allen Autoren könnte man einen CASE Fälle einbauen, um eine schöne Trennung mit Kommas zu erstellen wird es  mit Concat_ws umgesetzt
CREATE VIEW vKaufinfos AS SELECT b.BuchTitel AS Titel, CONCAT_WS(', ',a.AutorenName1,a.AutorenName2,a.AutorenName3) AS Autor, b.Sprache, b.aktuellerPreis AS Preis FROM tBuch AS b, tAutoren AS a WHERE b.AutorenID = a.AutorenID ORDER BY b.aktuellerPreis;

CREATE VIEW vTelefonBestellinfos AS SELECT b.ISBN, b.Verfuegbarkeit AS Verfuegbarkeit, b.Lieferfrist, v.VerlagName AS Verlag, CONCAT(v.VerlagKontaktVorname, ' ', v.VerlagKontaktName) AS Ansprechspartner, v.VerlagTelefonnummer FROM tBuch AS b, tVerlag AS v WHERE b.VerlagID = v.VerlagID ORDER BY b.Lieferfrist;

CREATE VIEW vEmailBestellinfos AS  SELECT b.ISBN, b.Verfuegbarkeit AS Verfügbarkeit, b.Lieferfrist, v.VerlagName AS Verlag, CONCAT(v.VerlagKontaktName, ' ', v.VerlagKontaktVorname) AS Ansprechspartner, v.VerlagMail AS Mailadresse FROM tBuch AS b, tVerlag AS v WHERE b.VerlagID = v.VerlagID ORDER BY b.Lieferfrist;

CREATE VIEW vBestandwertBuecher AS SELECT SUM(Einkaufspreis) AS BestandWertDerBuecher FROM tBuch;

-- Dupplikate der Buchtitel möglich da es untershciedliche bestellungen mit dem selben Buch geben kann
CREATE VIEW vBestellungenOverLieferfrist AS SELECT b.Buchtitel AS BuchtTitel, b.BuchID, a.BestellungsID, a.Bestellmenge FROM tBestellungen AS a, tBuch AS b WHERE  b.BuchID = a.BuchID AND b.Lieferfrist < a.Bestelldatum ORDER BY b.BuchID;

-- Alle bücher wo Besände unter 5 sind
CREATE VIEW v5Buecher AS SELECT b.Buchtitel , b.BuchID, d.AnzahlimLager, d.AnzahlimGestell FROM tBuch AS b, tDatenbank AS d WHERE d.DatenbankID = b.BuchID AND 5 > (d.AnzahlimLager + d.AnzahlimGestell);

-- (Lieber ANDs als JOINs) Alle bücher wo Besände unter 5 sind und bestellmenge grösser 2 angenommen das bei jeder Bestellung die Bestellmenge erhöht wird und das Datum aktuallisiert wird
CREATE VIEW v5BuecherOrderdMore30Days AS SELECT DISTINCT b.Buchtitel, b.BuchID, d.AnzahlimLager, d.AnzahlimGestell FROM tBuch AS b, tBestellungen AS t, tDatenbank AS d WHERE d.DatenbankID = b.BuchID AND 5 > (d.AnzahlimLager + d.AnzahlimGestell) AND t.Bestelldatum < '2019.10.10' AND t.BuchID = b.BuchID AND t.Bestellmenge >= 2 ORDER BY b.BuchID;

-- Alle bücher wo Besände unter 5 sind, die in den Letzten 30 Tage mehr als einmal nachbestellt wurden ohne datum aktiallisierung und änderungen der bestellmengen  Ausgabe nur von doppelten werten
CREATE VIEW v5BuecherOrderdMore30Days2 AS SELECT  b.Buchtitel, b.BuchID, d.AnzahlimLager, d.AnzahlimGestell FROM tBuch AS b, tBestellungen AS t, tDatenbank AS d WHERE d.DatenbankID = b.BuchID AND 5 > (d.AnzahlimLager + d.AnzahlimGestell) AND t.Bestelldatum < '2019.10.10' AND t.BuchID = b.BuchID GROUP BY b.Buchtitel,b.BuchID,d.AnzahlimLager,d.AnzahlimGestell HAVING COUNT(*)>1 ORDER BY b.BuchID;

--Alle Bücher mit nur einem Autor
CREATE VIEW vAllBuecherAutorJustHe AS  SELECT DISTINCT  b.Buchtitel, b.BuchID, a.AutorenName1 AS Autor FROM tBUch AS b, tAutoren AS a  WHERE a.AutorenName2 IS NULL AND a.AutorenName3 IS NULL AND b.AutorenID = a.AutorenID ORDER BY b.BuchID;

--für einen bestimmten Autor bsp 'Hatty Limming'
CREATE VIEW vHattyLimming AS SELECT DISTINCT  b.Buchtitel, b.BuchID, a.AutorenName1 AS Autor FROM tBUch AS b, tAutoren AS a  WHERE a.AutorenName2 IS NULL AND a.AutorenName3 IS NULL AND b.AutorenID = a.AutorenID AND a.AutorenName1 LIKE 'Hatty Limming' ORDER BY b.BuchID;

-- Bücher eines Verlags der nur Bücher von mehreren Autoren hat  Bücher wurden dafür abgeändert um einen Verlag zu erhalten der nur Bücher mit mehreren Autoren hat (Da ich so viele Bücher erzeugt hatte gab es vorher keinen Verlag mit den Vorgaben)
CREATE VIEW vVerlagWithJustMultipleAutorsForAllBooks AS SELECT DISTINCT v.VerlagName, b.BuchID,b.Buchtitel, CONCAT_WS(', ',a.AutorenName1,a.AutorenName2,a.AutorenName3) AS Autoren FROM tVerlag AS v, tBuch AS b, tAutoren AS a WHERE v.VerlagID NOT IN (SELECT DISTINCT b.VerlagID FROM tBuch AS b WHERE b.AutorenID NOT IN (SELECT a.AutorenID FROM tAutoren AS a WHERE a.AutorenName2 IS NOT NULL)) AND b.AutorenID = a.AutorenID AND v.VerlagID = b.VerlagID;
