
public class Aufgabe3 {

	public enum Ort {
		CHUR, GRUESCH, BONADUZ, UNTERVAZ,
		LANDQUART ,
		MAIENFELD, KLOSTERS; 
	}

	static int[][] graph;

	// Aufgabe a) F�llen des Graphen
	static void fillGraph() {
		// Folgende Verbindungen gibt es:
		// - Gr�sch-Klosters: 15
		// - Gr�sch-Chur: 10
		// - Chur-Bonaduz: 15
		// - Chur-Landquart: 13
		// - Chur-Untervaz: 10
		// - Chur-Maienfeld: 15
		// - Untervaz-Landquart: 7
		// - Untervaz-Maienfeld: 10
		// - Landquart-Maienfeld: 7
		graph = new int[7][7];
		
		// --> Vervollst�ndigen Sie hier
	}

	// Aufgabe b) Gibt es eine direkte Verbindung vom "from" nach "to"?
	static boolean edge(Ort from, Ort to) {
		// --> Vervollst�ndigen Sie hier
		return true;
	}

	// Aufgabe c) Ist der gegebene Pfad g�ltig
	// 
	// Beispiele:
	// - G�ltige Pfade: CHUR, BONADUZ, CHUR, MAIENFELD
	// - Ung�ltiger Pfade: KLOSTER, MAIENFELD (es gibt keine Verbindugn von
	// Klosters nach Maienfeld)
	static boolean validPath(Ort[] path) {
		// --> Vervollst�ndigen Sie hier
		return true;
	}

	// Aufgabe d) L�nge eines gegebenen Pfades
	// Beispiel: BONADUZ, CHUR, MAIENFELD hat die L�nge 30
	static int pathLength(Ort[] path) {
		// --> Vervollst�ndigen Sie hier
		return 0;
	}

	public static void main(String[] args) {
		// a) Graphen f�llen
		fillGraph();

		// b) Direkte Wege
		System.out.println("Gibt es einen direkten Weg von Chur nach Maienfeld?");
		System.out.println("Erwartetes Resultat: true");
		System.out.println("Berechnetes Resultat: " + edge(Ort.CHUR, Ort.MAIENFELD));
		System.out.println();

		System.out.println("Gibt es einen direkten Weg von Maienfeld nach Klosters?");
		System.out.println("Erwartetes Resultat: false");
		System.out.println("Berechnetes Resultat: " + edge(Ort.MAIENFELD, Ort.KLOSTERS));
		System.out.println();

		// c) G�ltiger Pfad
		System.out.println("Ist CHUR, BONADUZ, CHUR, MAIENFELD ein g�ltiger Pfad?");
		System.out.println("Erwartetes Resultat: true");
		Ort[] path1 = { Ort.CHUR, Ort.BONADUZ, Ort.CHUR, Ort.MAIENFELD };
		System.out.println("Berechnetes Resultat: " + validPath(path1));
		System.out.println();

		System.out.println("Ist KLOSTER, MAIENFELD ein g�ltiger Pfad?");
		System.out.println("Erwartetes Resultat: false");
		Ort[] path2 = { Ort.KLOSTERS, Ort.MAIENFELD };
		System.out.println("Berechnetes Resultat: " + validPath(path2));
		System.out.println();

		// d) Pfadl�nge
		System.out.println("Wie lange ist der Weg CHUR, BONADUZ, CHUR, MAIENFELD?");
		System.out.println("Erwartetes Resultat: 45");
		Ort[] path3 = { Ort.CHUR, Ort.BONADUZ, Ort.CHUR, Ort.MAIENFELD };
		System.out.println("Berechnetes Resultat: " + pathLength(path3));
		System.out.println();
	}
}
