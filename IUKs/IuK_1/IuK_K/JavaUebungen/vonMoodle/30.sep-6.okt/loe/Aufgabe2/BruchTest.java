package loesung;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.*;


public class BruchTest {

	@Test
	public void testBruch() {
		Bruch b = new Bruch(4,6);

		assertEquals(2, b.getZaehler());
		assertEquals(3, b.getNenner());
	}

	@Test
	public void testAdd() {
		Bruch b1 = new Bruch(4, 6);
		Bruch b2 = new Bruch(1, 3);
		Bruch sum = b1.add(b2);

		assertEquals(1, sum.getZaehler());
		assertEquals(1, sum.getNenner());
	}

	@Test
	public void testMul() {
		Bruch b1 = new Bruch(4, 6);
		Bruch b2 = new Bruch(1, 3);
		Bruch prod = b1.mul(b2);

		assertEquals(2, prod.getZaehler());
		assertEquals(9, prod.getNenner());
	}

	@Test
	public void testGetZaehler() {
		Bruch b = new Bruch(5,3);
		
		assertEquals(5, b.getZaehler());
	}

	@Test
	public void testGetNenner() {
		Bruch b = new Bruch(5,3);
		
		assertEquals(3, b.getNenner());
	}
}
