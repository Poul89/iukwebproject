
public class ComplexNumber {
	private double real;
	private double imaginary;
	
	public ComplexNumber() {
		this.real = 0;
		this.imaginary = 0;
	}

	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}
		
	public double getReal() {
		return real;
	}
	
	public double getImaginary() {
		return imaginary;
	}
	
	public ComplexNumber add(ComplexNumber number) {
		ComplexNumber result = new ComplexNumber();
		result.real = this.real + number.real;
		result.imaginary = this.imaginary + number.imaginary;
		return result;
	}
	
	public double abs() {
		return java.lang.Math.sqrt(real*real + imaginary*imaginary);
	}
	
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(real);
		if (imaginary>=0) {
			buf.append("+");
		}
		buf.append(imaginary);
		buf.append("i");
		return buf.toString();
	}
}
