
public class Modulo10Rekursiv {
	private static int[] KOMBINATIONSZEILE = {0,9,4,6,8,2,7,1,3,5};
	private static int[][] KOMBINATIONSTABELLE;
	static {
		KOMBINATIONSTABELLE = new int[KOMBINATIONSZEILE.length][KOMBINATIONSZEILE.length];
		for (int zeile=0;zeile<KOMBINATIONSZEILE.length; zeile++) {
			for (int spalte = 0; spalte<KOMBINATIONSZEILE.length; spalte ++) {
				int spalteInZeile = (spalte+zeile) %KOMBINATIONSZEILE.length; 
				KOMBINATIONSTABELLE[zeile][spalte] = KOMBINATIONSZEILE[spalteInZeile];
			}
		}
	}
	

	public static boolean pruefe(String ziffernreihe) {
		int letzteZiffer = Character.digit(ziffernreihe.charAt(ziffernreihe.length()-1),10);
		return letzteZiffer == pruefziffer(ziffernreihe.substring(0, ziffernreihe.length()-1));
	}


	public static int pruefziffer(String ziffernreihe) {
		int uebertrag = 0;
		for (int index=0; index<ziffernreihe.length(); index++) {
			int ziffer = Character.digit(ziffernreihe.charAt(index), 10);
			uebertrag = KOMBINATIONSTABELLE[ziffer][uebertrag]; 
		}
		return (10-uebertrag) % 10;
	}
}
