class ListElementEinfach {

    Object obj;

    ListElementEinfach nextElem;

    public ListElementEinfach(Object obj) {
        this.obj = obj;
        nextElem = null;
    }

    public void setNextElem(ListElementEinfach nextElem) {
        this.nextElem = nextElem;
    }

    public ListElementEinfach getNextElem() {
        return nextElem;
    }

    public Object getObj() {
        return obj;
    }
}