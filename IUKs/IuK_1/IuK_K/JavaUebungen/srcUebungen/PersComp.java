class PersonComparator implements java.util.Comparator {

	public int compare(Object o1, Object o2) {
		Person2 p1 = (Person2) o1;
		Person2 p2 = (Person2) o2;
		if (p1 == p2) {
			return 0;
		}
		int res = p1.name.compareTo(p2.name);
		if (res != 0) {
			return res;
		}
		return p1.vorname.compareTo(p2.vorname);
	}
}

public class PersComp {
	public static void main(String[] arg) {
		Person2[] p = new Person2[5];
		p[0] = new Person2("Mueller", "Max");
		p[1] = new Person2("Mueller", "Martin");
		p[2] = new Person2("Meier", "Martin");
		p[3] = new Person2("Meier", "Max");
		p[4] = new Person2("Gueller", "Gax");

		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
		System.out.println();
		System.out.println("Sortiert:");
		java.util.Arrays.sort(p, new PersonComparator());
		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
	}
}
