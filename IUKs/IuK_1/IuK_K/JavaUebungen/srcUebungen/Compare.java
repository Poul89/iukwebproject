class ComparablePerson extends Person2 implements Comparable {

    ComparablePerson(String name, String vorname) {
        super(name, vorname);
    }

    public int compareTo(Object o) {
        Person2 p = (Person2) o;
        int res = this.name.compareTo(p.name);
        if (res != 0) {
            return res;
        }
        return this.vorname.compareTo(p.vorname);
    }
}

class CompPerson {
    public static void main(String[] arg) {
        Person2[] p = new ComparablePerson[5];
        p[0] = new ComparablePerson("Mueller", "Max");
        p[1] = new ComparablePerson("Mueller", "Martin");
        p[2] = new ComparablePerson("Meier", "Martin");
        p[3] = new ComparablePerson("Meier", "Max");
        p[4] = new ComparablePerson("Gueller", "Gax");

        for (int i = 0; i < p.length; i++) {
            System.out.println(p[i]);
        }
        System.out.println();
        System.out.println("Sortiert:");
        java.util.Arrays.sort(p);
        for (int i = 0; i < p.length; i++) {
            System.out.println(p[i]);
        }
    }
}

/*
public class CompareToExample {
   public static void main(String args[]) {
       String str1 = "String method tutorial";
       String str2 = "compareTo method example";
       String str3 = "String method tutorial";

       int var1 = str1.compareTo( str2 );
       System.out.println("str1 & str2 comparison: "+var1);

       int var2 = str1.compareTo( str3 );
       System.out.println("str1 & str3 comparison: "+var2);

       int var3 = str2.compareTo("compareTo method example");
       System.out.println("str2 & string argument comparison: "+var3);
   }
}

str1 & str2 comparison: -16
str1 & str3 comparison: 0
str2 & string argument comparison: 0

Casesensitive

public class JavaExample {
   public static void main(String args[]) {
	//uppercase
	String str1 = "HELLO";
	//lowercase
	String str2 = "hello";;

	System.out.println(str1.compareTo(str2));
   }

}

=> -32


 */