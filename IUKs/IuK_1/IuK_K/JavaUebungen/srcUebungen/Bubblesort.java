
public class Bubblesort {
	static void bubble(int[] a) {
		int len = a.length - 1;
		for (int k = len; k>0; k--) {
			boolean changed = false;
			for (int i=0; i<k; i++) {
				if (a[i] > a[i+1]) {
					swap(a, i, i+1);
					changed = true;
				}
			}
			if (!changed) {
				break;
			}
		}
	}
	
	static void swap(int[] a, int i, int j) {
		int temp = a[i];
		a[i] = a[j];
		a[j] = temp;
	}
	
	public static void main(String[] arg) {
		// bubble
		int[] a1 = new int[]{20, 11, 6, 10, 3};
		System.out.println("BubbleSort:");
		bubble(a1);
		System.out.println(java.util.Arrays.toString(a1));
		
	}
}
