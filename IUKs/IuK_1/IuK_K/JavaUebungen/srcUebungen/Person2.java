
public class Person2 {
	String name;
	String vorname;

	Person2(String name, String vorname) {
		this.name = name;
		this.vorname = vorname;
	}

	public String toString() {
		return name + " " + vorname;
	}

}
