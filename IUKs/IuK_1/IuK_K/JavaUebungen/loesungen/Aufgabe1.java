class ComparablePerson extends Person implements Comparable {

	ComparablePerson(String name, String vorname) {
		super(name, vorname);
	}

	public int compareTo(Object o) {
		Person p = (Person) o;
		int res = this.name.compareTo(p.name);
		if (res != 0) {
			return res;
		}
		return this.vorname.compareTo(p.vorname);
	}
}

public class Aufgabe1 {
	public static void main(String[] arg) {
		Person[] p = new ComparablePerson[5];
		p[0] = new ComparablePerson("Mueller", "Max");
		p[1] = new ComparablePerson("Mueller", "Martin");
		p[2] = new ComparablePerson("Meier", "Martin");
		p[3] = new ComparablePerson("Meier", "Max");
		p[4] = new ComparablePerson("Gueller", "Gax");

		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
		System.out.println();
		System.out.println("Sortiert:");
		java.util.Arrays.sort(p);
		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
	}
}

