class PersonComparator implements java.util.Comparator {

	public int compare(Object o1, Object o2) {
		Person p1 = (Person) o1;
		Person p2 = (Person) o2;
		if (p1 == p2) {
			return 0;
		}
		int res = p1.name.compareTo(p2.name);
		if (res != 0) {
			return res;
		}
		return p1.vorname.compareTo(p2.vorname);
	}
}

public class Aufgabe2 {
	public static void main(String[] arg) {
		Person[] p = new Person[5];
		p[0] = new Person("Mueller", "Max");
		p[1] = new Person("Mueller", "Martin");
		p[2] = new Person("Meier", "Martin");
		p[3] = new Person("Meier", "Max");
		p[4] = new Person("Gueller", "Gax");

		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
		System.out.println();
		System.out.println("Sortiert:");
		java.util.Arrays.sort(p, new PersonComparator());
		for (int i = 0; i < p.length; i++) {
			System.out.println(p[i]);
		}
	}
}
