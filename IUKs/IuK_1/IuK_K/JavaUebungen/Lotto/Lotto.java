
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.TitledBorder;

/**
 * Find Lotto Numbers
 *
 * @author Rico Pauli
 * swissdragons@hotmail.com
 * @version ve 1.0.2016
 * Date : 30.10.2016
 */
public class Lotto
{

    JButton starter;
    JTextArea posting;
    JTextArea countext;
    JFrame wind;
    JButton chooser;
    JButton reset;
    protected int count = 0;
    int juse = 0;

    public  Lotto (){

        this.face();

    }

    public void  botton(){
        starter.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                //starter.setText("");
                String x = posting.getText();
                posting.setText("");
                juse++;
                calcu();
                posting.setText(posting.getText() + "\n" + x);  // show more
            }
        } );
        reset.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                posting.setText("");
                countext.setText("");
                juse = 0;

            }
        } );
    }

    public void face(){
        wind = new JFrame("Lotto Number Selector");
        Container mid = wind.getContentPane();

        mid.setLayout(new BorderLayout());
        chooser = new JButton("Good Luck");
        starter = new JButton("Start");
        reset = new JButton("Reset");

        starter.setBackground(Color.yellow);
        chooser.setBackground(Color.green);
        reset.setBackground(Color.gray);

        JPanel centerpanel = new JPanel(new BorderLayout());
        JPanel startnr = new JPanel(new BorderLayout());

        posting = new JTextArea();
        countext = new JTextArea();
        countext.setFont(new Font("Autumn", Font.PLAIN, 40));
        starter.setFont(new Font("Autumn", Font.PLAIN, 40));
        reset.setFont(new Font("Autumn", Font.PLAIN, 20));
        chooser.setFont(new Font("Autumn", Font.PLAIN, 40));

        countext.setBackground(Color.red);
        countext.repaint();
        posting.setBorder (new TitledBorder ("Your Numbers: "));
        countext.setBorder (new TitledBorder ("Count"));
        posting.setFont(new Font("Areal", Font.PLAIN, 20));

        mid.add(centerpanel);
        centerpanel.add(chooser , BorderLayout.NORTH);
        centerpanel.add(startnr , BorderLayout.CENTER);
        centerpanel.add(posting , BorderLayout.SOUTH);

        startnr.add(starter , BorderLayout.WEST);
        startnr.add(reset , BorderLayout.CENTER);
        startnr.add(countext , BorderLayout.EAST);

        wind.setPreferredSize(new Dimension(300, 500));
        chooser.setPreferredSize(new Dimension(300, 80));
        posting.setPreferredSize(new Dimension(300, 300));
        countext.setPreferredSize(new Dimension(60, 80));
        starter.setPreferredSize(new Dimension(140, 80));

        wind.pack();
        wind.setVisible(true);
        this.botton();

    }

    public void calcu(){
        boolean unsort = true;
        int [] numb = new int [6];
        numb[0] = (int)(1 + Math.round(Math.random()*41));
        for (int i = 1; i < 6; i++){ // create 6 diffrent numbers
            numb[i] = (int)(1 + Math.round(Math.random()*41));
            for (int b = 0; b < i; b++){
                if(numb[i]== numb[b]){
                    i = i-1;
                }
            }
        }

        while(unsort){ // sort number
            unsort = false;
            for(int i = 0; i < 5; i++){
                int a;
                if(numb[i]>numb[i+1]){
                    a = numb[i];
                    numb[i] = numb[i+1];
                    numb[i+1] = a;
                    unsort = true;
                }

            }
        }

        for(int i = 0; i < 6; i++){

            posting.setText(posting.getText() + " " +numb[i]);
        }
        countext.setText(""+juse);

    }

    public static void main (String[] args){
        new Lotto();

    }
}
