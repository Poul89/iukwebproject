public class TestPerson {
    public static void main(String[] args) {
        String vorname;
        String name;
        ClassesAndObject schoettle;
        schoettle = new ClassesAndObject();
        schoettle.setName("Schöttle");
        schoettle.setVorname("Lothar");
        name = schoettle.getName();
        vorname = schoettle.getVorname();
        System.out.println("Vorname: " + vorname);
        System.out.println("Name: " + name);
    }
}
