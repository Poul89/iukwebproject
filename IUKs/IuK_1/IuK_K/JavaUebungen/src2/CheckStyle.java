public class CheckStyle {

    String name;
    String vorname;
    String strasseAndNummer;
    int pLZ;
    String ort;

    CheckStyle(String n, String v, String s, int p, String o) {
        name = n;
        vorname = v;
        strasseAndNummer = s;
        pLZ = p;
        ort = o;
    }


    void println() {
        System.out.println(vorname + " " + name);
        System.out.println(strasseAndNummer);
        System.out.println(pLZ + " " + ort);
    }

}
