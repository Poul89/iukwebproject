
public class Fiboagain {

    public static void fibo(int endzahl) { // iterativ

        long a = 1;
        long b = 1;
        long fibozahl;

        while (endzahl > 0) {
            fibozahl = a + b;
            a = b;
            b = fibozahl;
            endzahl = endzahl - 1;
            System.out.println(b);
        }
    }

    public int fibonacci(int n) { // Rekursiv
        if (n <= 1) {
            return n;
        } else {
            return fibonacci(n - 1) + fibonacci(n - 2);

        }

    }

    public void erkennen(int v) {
        double c = 0;

        double a = 0.2;
        int k = 4;
        while (v >= 0) {
            c = k * a * (1 - a);
            a = c;
            System.out.println(c);
            v = v - 1;
        }
    }

    public static void main(String[] args) {
        Fiboagain fib = new Fiboagain();
        // fibo(50);
        System.out.println(fib.fibonacci(10));
        fib.erkennen(40);
    }
}