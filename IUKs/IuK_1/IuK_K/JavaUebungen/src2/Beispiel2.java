public class Beispiel2{

    public static void main(String[] args) {

        int i = 1;
        increment(i);
        System.out.println(i); // was wird ausgegeben?

        Integer i2 = 1;
        increment(i2);
        System.out.println(i2); // was wird ausgegeben?
    }

    public static void increment(int i) {
        i++;
    }

    public static void increment(Integer i) {
        i++;
    }
}
