import java.math.BigDecimal;

public class Math22 {
    public static void main(String[] args) {
        double d1 = 0.1;
        double d2 = 0.2;
        double d3 = d1 + d2;
        boolean b1 = d3 == 0.3;
        System.out.println("d1=" + d1 + " d2=" + d2 + " d3=" + d3);
        BigDecimal bd1 = new BigDecimal(d1);
        BigDecimal bd2 = new BigDecimal(d2);
        BigDecimal bd3 = new BigDecimal(d3);
        System.out.println("beste Darstellbare Zahl als double von 0.1  ist 0.1 in Wirklichkeit ist aber 0.1:");
        System.out.println("d1=" + bd1 + " d2=" + bd2+" bd3="+bd3);
    }

}
