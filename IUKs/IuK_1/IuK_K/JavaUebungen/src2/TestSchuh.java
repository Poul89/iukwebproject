public class TestSchuh {
    public static void main(String[] args) {
        Schuh s = new Schuh();
        s.print();
        s.setGroesse(42);
        s.setHersteller("Mike");
        s.setModellbezeichnung("Air Ultimatics");
        s.print();
        System.out.println(s.getGroesse() + " " + s.getHersteller() + " " + s.getModellbezeichnung() + " get Class: " + s.getClass() + " hascode: " + s.hashCode());
    }
}
