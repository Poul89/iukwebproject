package Schildkrot;

import junit.framework.TestCase;

/**
 * JUnit-Testklasse für SchildkroeteTest.
 * 
 * @author pape
 *
 */
public class SchildkroeteTest extends TestCase {

	/*
	 * Test method for 'de.hska.java.aufgaben.grafik.Chelone.getRichtung()'
	 */
	public void testGetRichtung() {
		Chelone schildkroete = new Chelone(0,0);
		schildkroete.drehen(180.0);
		assertEquals(Math.PI, schildkroete.getRichtung(), 0.00001);
	}

	public void testLaufen(){
		Chelone schildkroete = new Chelone(0,0);
		schildkroete.laufen(10.0);
		
		assertEquals(10.0, schildkroete.getX(), 0.00001);
		assertEquals(0.0, schildkroete.getY(), 0.00001);
	}
	
	public void testLaufen1(){
		Chelone schildkroete = new Chelone(0,0);
		schildkroete.drehen(45.0);
		schildkroete.laufen(Math.sqrt(2.0));
		
		assertEquals(1.0, schildkroete.getX(), 0.00001);
		assertEquals(1.0, schildkroete.getY(), 0.00001);
	}

	public void testLaufen2(){
		Chelone schildkroete = new Chelone(0,0);
		schildkroete.drehen(45.0);
		schildkroete.laufen(10.0);
		
		assertEquals(Math.sqrt(50.0), schildkroete.getX(), 0.00001);
		assertEquals(Math.sqrt(50.0), schildkroete.getY(), 0.00001);
	}

	public void testDrehen(){
		Chelone schildkroete = new Chelone(0,0);
		schildkroete.drehen(360.0);
		schildkroete.laufen(10.0);
		
		assertEquals(10.0, schildkroete.getX(), 0.00001);
		assertEquals(0.0, schildkroete.getY(), 0.00001);
	}
	
	public void testDrehen1(){
		Chelone schildkroete = new Chelone(0,0);
		schildkroete.drehen(90.0);
		schildkroete.laufen(5.0);
		
		assertEquals(0.0, schildkroete.getX(), 0.00001);
		assertEquals(5.0, schildkroete.getY(), 0.00001);
	}
}
