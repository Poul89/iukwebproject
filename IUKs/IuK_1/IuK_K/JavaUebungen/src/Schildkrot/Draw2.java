package Schildkrot;


public class Draw2 {

    public static void main(String[] args) {
        SchildkroetenGrafik schildkroetenGrafik = new SchildkroetenGrafik("Kochkurve", 1500, 1000);
        Schildkroete schildkroete = schildkroetenGrafik.createSchildkroete();
        schildkroete.positionieren(500, 500);

       // kochkurveZeichnen(schildkroete, 800);
        drachenkurveZeichnen(schildkroete, 20,-1);
        schildkroetenGrafik.warten();
    }

    private static void kochkurveZeichnen(Schildkroete schildkroete, double laenge) {
        if (laenge <= 0.01) {
            schildkroete.laufen(laenge);
        } else {
            kochkurveZeichnen(schildkroete, laenge / 3.0);
            schildkroete.drehen(60.0);
            kochkurveZeichnen(schildkroete, laenge / 3.0);
            schildkroete.drehen(-120.0);
            kochkurveZeichnen(schildkroete, laenge / 3.0);
            schildkroete.drehen(60.0);
            kochkurveZeichnen(schildkroete, laenge / 3.0);
        }
    }

    private static void drachenkurveZeichnen(Schildkroete schildkroete, int dimension, int drehrichtung) {
                  if (dimension == 0) {
                                      schildkroete.laufen(1);
                               } else {
                                       drachenkurveZeichnen(schildkroete, dimension - 1, 1);
                          schildkroete.drehen(drehrichtung * -85);
                                      drachenkurveZeichnen(schildkroete, dimension - 1, -1);
                              }
                 }
}



























































