package Schildkrot;


public class Draw1 {

    public static void main(String[] args) {
        SchildkroetenGrafik schildkroetenGrafik = new SchildkroetenGrafik("Dies ist das Haus vom Nikolaus", 400, 300);

        Schildkroete schildkroete = schildkroetenGrafik.createSchildkroete();


        schildkroete.positionieren(50, 200);
        schildkroete.geheZu(100, 200);
        schildkroete.geheZu(100, 100);
        schildkroete.geheZu(50, 200);
        schildkroete.geheZu(50, 100);
        schildkroete.geheZu(100, 100);
        schildkroete.geheZu(75, 50);
        schildkroete.geheZu(50, 100);
        schildkroete.geheZu(100, 200);


        schildkroetenGrafik.warten();
    }


}
