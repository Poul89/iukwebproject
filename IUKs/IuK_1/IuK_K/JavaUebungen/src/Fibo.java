public class Fibo {
    static int fib(int n) { // nicht effizient
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return fib(n - 1) + fib(n - 2);
        }
    }

    static int fibDynIter(int[] fib, int n) {

        for (int i = 2; i <= n; i++) {
            fib[i] = fib[i - 1] + fib[i - 2];
        }
        return fib[n];
    }

    static int fibDynRek(int[] fib, int n) {
        if (fib[n] >= 0) {
            return fib[n];
        }
        fib[n] = fibDynRek(fib, n - 1) + fibDynRek(fib, n - 2);
        return fib[n];
    }

    public static void main(String[] args) {
        int n = 8;
        System.out.println("fib(" + n + ")=" + fib(n));
    }
    // Phi 1.618033 fibinatschi zahlen verhältnisse  , Kreis PI !!!!  fib[i]/fib[i-1]
    // Goldener Schnitt
}
