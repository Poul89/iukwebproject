public class Fakultaet {
    public static void main(String[] args) {
        System.out.println(fakultaet(5));
    }
    private static int fakultaet(int n) {
        if (n <= 1) {
            return 1;
        } else {
            return fakultaet(n - 1) * n;
        }
    }
}


//  Iterativ
// public class Fakultaet {
//    public static void main(String[] args) {
//        System.out.println(fakultaet(5));
//    }
//    private static int fakultaet(int n) {
//        int i = 1;
//        int result = 1;
//        while(i<n) {
//            result *= (++i);
//        }
//        return result;
//    }
//}