public class TempoClass2 {


    public static void main(String[] args) {
        long timeStart = System.nanoTime();
        int c = 0;
        for (int i = 0; i < 1000000; i++) {
            // nichts machen
            for (int ic = 0; ic < 1000000; ic++) {
                c = c + 1;
            }
        }
        long timeEnd = System.nanoTime();
        long nanotime = timeEnd - timeStart;
        System.out.println("Verlaufszeit der Schleife: " + nanotime + " Nanosek.");
        long militime = nanotime/1000000;
        System.out.println("Verlaufszeit der Schleife: " + militime + " Millisek.");
        long sectime = militime/1000;
        System.out.println("Verlaufszeit der Schleife: " + sectime + " Sek.");
    }
}