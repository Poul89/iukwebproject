

public class Aufgabe7 {
    // f�r jede Richtung 90 �
    static void notRekStrecke(Dialog dialog, int x1, int y1, int x2, int y2) {

        while (x1 != x2 || y1 != y2) {

            if (x1 < x2) {
                if (y1 == y2) {
                    dialog.markiere(x1++, y1);
                }
                if (y1 < y2) {
                    dialog.markiere(x1++, y1++);
                } else {
                    dialog.markiere(x1++, y1--);
                }
            } else {
                if (y1 == y2) {
                    dialog.markiere(x1--, y1);
                }
                if (y1 < y2) {
                    dialog.markiere(x1--, y1++);
                } else {
                    dialog.markiere(x1--, y1--);
                }
            }

        }

    }



  static void notRekStreckeAll(Dialog dialog, int x1, int y1, int x2, int y2) {
        int x11 = x1;
        int y11 = y1;
        int x22 = x2;
        int y22 = y2;
        while (x11 != x22 && y11 != y22) {

            float absx = Math.abs(x22 - x11);
            float absy = Math.abs(y22 - y11);
            float minx = x22 - x11;
            float miny = y22 - y11;
            float x;
            float y;
            float indix = 0;
            float indiy = 0;

            if (minx == 0) {
                minx = 1;
            }
            if (miny == 0) {
                miny = 1;
            }
            if (minx < 0) {
                minx = minx * -1;
                indix = 1;
            }
            if (miny < 0) {
                miny = miny * -1;
                indiy = 1;
            }
            if (absx >= absy) {

                x = (minx / absx);
                y = (miny / absx);
                if (indix == 1) {
                    x = x * -1;
                }
                if (indiy == 1) {
                    y = y * -1;
                }

            } else {
                x = (minx / absy);
                y = (miny / absy);
                if (indix == 1) {
                    x = x * -1;
                }
                if (indiy == 1) {
                    x = y * -1;
                }

            }


            if (x == 1) {
                if (y >= -0.5 && y <= 0.5) {

                   x11 = x11 + 1;
                }
                if (y >= 0.5) {
                   x11= x11 + 1;
                   y11 = y11 + 1;

                }
                if (y <= -0.5) {
                    x11 = x11 - 1;
                    y11 = y11 - 1;
                }
            }
            if (x == -1) {
                if (y >= -0.5 && y <= 0.5) {
                    x11 = x11 - 1;
                }
                if (y >= 0.5) {
                   x11 = x11 - 1;
                   y11 = y11 + 1;
                }
                if (y <= -0.5) {
                   x11 = x11 - 1;
                   y11 = y11 - 1;
                }
            }
            if (y == 1) {

                if (x >= -0.5 && x <= 0.5) {
                   y11 = y11 + 1;
                }
                if (x >= 0.5) {
                    x11 = x11 + 1;
                    y11 = y11 + 1;
                }
                if (x <= -0.5) {
                   x11 = x11 - 1;
                   y11 = y11 + 1;
                }
            }
            if (y == -1) {
                if (x >= -0.5 && x <= 0.5) {
                  y11 = y11 - 1;
                }
                if (x >= 0.5) {
                    x11 = x11 + 1;
                    y11 = y11 - 1;
                }
                if (x <= -0.5) {
                   x11= x11 - 1;
                   y11 = y11 - 1;
                }
            }

            dialog.markiere(x11, y11);
        }

    }

    // Rekursiv jede richtung 90�
    static void streckeRek(Dialog dialog, int x1, int y1, int x2, int y2) {
        // achtung vers�tze bei schr�gen linien  <= oder mit || arbeiten
        if (Math.abs(x1 - x2) <= 1 && Math.abs(y1 - y2) <= 1) { // x1 == x2 and y1 == y2
            dialog.markiere(x1, y1);
            // nach l�sung wird x2, y2 markiert ist das notwendid ? , die punkte vorher sollten ja schon gesetzt sein da <= 1
        } else {
            // if(x1) { alle achsen ?
            int mx;
            int my;
            mx = (x1 + x2) / 2;
            my = (y1 + y2) / 2;
            streckeRek(dialog, x1, y1, mx, my);
            streckeRek(dialog, mx, my, x2, y2);
            // }
        }

    }

    public static void main(String[] args) {
        Dialog dialog = new Dialog();
        System.out.println((long) (-1 / 90));
        // Zeichne eine Linie von (10,10) nach (100,100)

        // Nicht rekursiv, sondern einfach Punkt f�r Punkt zeichnen
        // Umgebaut f�r jede Richtung 90� !!
        notRekStreckeAll(dialog, 10, 200, 200, 500);

        // for (int i = 10; i < 100; i++) {
        //   dialog.markiere(i, i);
        //}

        // Zeichne eine Linie von (10,100) nach (100,10)
        // Rekursive L�sung
        // streckeRek(dialog, 1000, 500, 300, 200);


        //  notRekStreckeAll(dialog, 10, 10, 100, 20);
    }

}
