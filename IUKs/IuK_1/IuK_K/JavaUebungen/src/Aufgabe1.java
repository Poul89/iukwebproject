import java.util.Arrays;
import java.util.Random;

public class Aufgabe1 {
    public static void main(String[] args) {
        int ic = 0;
        while (ic < 10) {
            final int size = 100000000;
            Random random = new Random();
            int[] bigArray = new int[size];
            for (int i = 0; i < bigArray.length; i++) {
                bigArray[i] = random.nextInt();
            }


            long start = System.currentTimeMillis();


            Arrays.sort(bigArray);
            long stop = System.currentTimeMillis();
            System.out.println("Time: " + (stop - start) + " ms");
            //200mil
            // Time: 18066 ms
            //Time: 17775 ms
            //Time: 17939 ms
            //Time: 17824 ms
            //Time: 17924 ms
            //Time: 18053 ms
            //Time: 17973 ms
            //Time: 18062 ms
            //Time: 18000 ms
            //Time: 17989 ms
            //100mil
            // Time: 8938 ms
            //Time: 9022 ms
            //Time: 8981 ms
            //Time: 8951 ms
            //Time: 8953 ms
            //Time: 10747 ms
            //Time: 10292 ms
            //Time: 9577 ms
            //Time: 9582 ms
            //Time: 9749 ms

            ic++;
        }


    }
}