// Datei: TestKonstruktoren.java

public class TestKonstruktoren
{
   public static void main (String[] args)
   {
      System.out.println ("Exemplar von A wird angelegt");
      A aRef = new A();
      System.out.println();

      System.out.println ("Exemplar von B wird angelegt");
      B bRef = new B();
      System.out.println();

      System.out.println ("Exemplar von C wird angelegt");
      C cRef = new C();
      System.out.println();
   }
}
