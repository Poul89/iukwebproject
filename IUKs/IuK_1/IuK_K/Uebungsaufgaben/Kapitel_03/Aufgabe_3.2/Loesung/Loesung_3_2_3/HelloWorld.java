// Datei: HelloWorld.java

public class HelloWorld // Klasse zur Ausgabe von "Hello, world!"
{
   public static void main (String[] args) // Methode main()
   {
      // zur Ausgabe der Zeichenkette
      System.out.println ("Hello, world!");
   }
}