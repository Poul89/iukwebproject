// Datei: ZuweisungsOperator.java

public class ZuweisungsOperator
{
   public static void main(String[] args)
   {
      int a = 39;
      int b = 5;

      while (a >= b)
         a -= b;

      System.out.println("39 Modulo 5 ist: " + a);
   }
}
