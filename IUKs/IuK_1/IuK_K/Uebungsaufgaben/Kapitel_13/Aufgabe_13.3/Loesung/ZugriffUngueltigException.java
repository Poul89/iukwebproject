// Datei: ZugriffUngueltigException.java

public class ZugriffUngueltigException extends Exception {

   public ZugriffUngueltigException (String string)
   {
      super (string);
   }
}
