// Datei: KeineBerechtigungException.java

public class KeineBerechtigungException extends Exception {

   public KeineBerechtigungException (String string)
   {
      super (string);
   }
}
