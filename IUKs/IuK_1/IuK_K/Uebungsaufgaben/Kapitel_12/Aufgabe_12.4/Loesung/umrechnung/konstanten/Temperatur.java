// Datei: Temperatur.java

package umrechnung.konstanten;

public class Temperatur
{
   public static final float faktorFahrenheitNachCelsius = 5 / 9f;
   public static final float summandFahrenheitNachCelsius = 32f;
   public static final float faktorCelsiusNachFahrenheit = 9 / 5f;
   public static final float summandCelsiusNachFahrenheit = 32f;
}