// Datei: GabelBelegtException.java

public class GabelBelegtException extends Exception
{
   private static final long serialVersionUID = 1L;
   public GabelBelegtException()
   {
      super ("Gabel ist belegt!");
   }
}
