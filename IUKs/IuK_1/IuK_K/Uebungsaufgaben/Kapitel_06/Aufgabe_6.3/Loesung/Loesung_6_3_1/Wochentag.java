// Datei: Wochentag.java

public enum Wochentag
{
   MONTAG,
   DIENSTAG,
   MITTWOCH,
   DONNERSTAG,
   FREITAG,
   SAMSTAG,
   SONNTAG
}