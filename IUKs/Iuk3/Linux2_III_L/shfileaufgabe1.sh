#!/bin/bash
#this script converts all Umlauts in the given LaTeX file into the proper format
SCRIPTNAME="$0"
SEDSCRIPT='s/ä/\\\"a/g;'
SEDSCRIPT=$SEDSCRIPT's/Ä/\\\"A/g;'
SEDSCRIPT=$SEDSCRIPT's/ö/\\\"o/g;'
SEDSCRIPT=$SEDSCRIPT's/Ö/\\\"O/g;'
SEDSCRIPT=$SEDSCRIPT's/ü/\\\"u/g;'
SEDSCRIPT=$SEDSCRIPT's/Ü/\\\"U/g;'
SEDSCRIPT_READABLE=$SEDSCRIPT's/\\\"\(.\)/\"\1/g;' #convert backslash form to readable form
SEDSCRIPT_READABLE=$SEDSCRIPT_READABLE's/{\"\(.\)}/\"\1/g;' #convert bracket form to readable form
SEDSCRIPT=$SEDSCRIPT's/\([^\\]\)\"a/\1\\\"a/g;'
SEDSCRIPT=$SEDSCRIPT's/\([^\\]\)\"A/\1\\\"A/g;'
SEDSCRIPT=$SEDSCRIPT's/\([^\\]\)\"o/\1\\\"o/g;'
SEDSCRIPT=$SEDSCRIPT's/\([^\\]\)\"O/\1\\\"O/g;'
SEDSCRIPT=$SEDSCRIPT's/\([^\\]\)\"u/\1\\\"u/g;'
SEDSCRIPT=$SEDSCRIPT's/\([^\\]\)\"U/\1\\\"U/g;'
SEDSCRIPT=$SEDSCRIPT's/\([^{]\)\\\"\(.\)\([^}]\)/\1{\\\"\2}\3/g;' #create bracket form
SEDSCRIPT_UMLAUTS=$SEDSCRIPT_READABLE #start with readable form
SEDSCRIPT_UMLAUTS=$SEDSCRIPT_UMLAUTS's/"a/ä/g;'
SEDSCRIPT_UMLAUTS=$SEDSCRIPT_UMLAUTS's/"A/Ä/g;'
SEDSCRIPT_UMLAUTS=$SEDSCRIPT_UMLAUTS's/"o/ö/g;'
SEDSCRIPT_UMLAUTS=$SEDSCRIPT_UMLAUTS's/"O/Ö/g;'
SEDSCRIPT_UMLAUTS=$SEDSCRIPT_UMLAUTS's/"u/ü/g;'
SEDSCRIPT_UMLAUTS=$SEDSCRIPT_UMLAUTS's/"U/Ü/g;'

FLAGS=""

#test for OS
WINFLAG="$(echo $OS | grep -ci win)"
	if [ 0 != $WINFLAG ]; then
		EDITORCMD="/cygdrive/d/opt/TeXstudio/texstudio.exe"
	else
		EDITORCMD="texstudio"
	fi


#function to show usage message
usage ()
{
    echo >&2 "$(basename $SCRIPTNAME) - In a real bash script, there would be a description of the script and infoermation about how to use it
usage: $(basename $SCRIPTNAME) <options> <files>
	Options:
		-opt1: some explanation
		-opt2: more explanation
		-opt3: even more explanation"

    exit 1
}

killSingle ()
{
  FILE=$1
  echo "Working on $FILE" >&2

  #test for file
  if [ ! -f $FILE ]; then
    echo "$FILE does not exist as a file" >&2
    usage
  fi
  #test charset conversion
  iconv -t UTF-8//TRANSLIT "$FILE" > "$FILE.bak"
  if [ 0 -ne $? ]; then
		echo "Problem forward converting $FILE to UTF-8"
		ERRORLINE="$(cat $FILE.bak | wc -l)"
		ERRORLINE=$((ERRORLINE + 1))
		echo "Line: $ERRORLINE"
		#start editor
		$EDITORCMD $FILE --line $ERRORLINE
		exit 1
  fi
  #filter
  cat "$FILE.bak" | sed "${SEDSCRIPT}" > "$FILE.sed"
  if [ 0 -ne $? ]; then
		echo "Problem to filter $FILE"
		exit 1
  fi
  rm "$FILE"
  mv "$FILE.sed" "$FILE"
  rm "$FILE.bak"
}

#evaluate parameters
set -- `getopt ehu "$@"`
[ $# -lt 1 ] && exit 1	# getopt failed
while [ $# -gt 0 ]
do
	case "$1" in
		-e)	SEDSCRIPT=$SEDSCRIPT_READABLE;;
		-h)	usage;;
		-u)	SEDSCRIPT=$SEDSCRIPT_UMLAUTS;;
		--)	shift; break;;
		*)	break;;		# terminate while loop
	esac
	shift
done

if [ "0" -lt $# ]; then
	FILES="$*"
else
	FILES=$(find . -type f -iname '*.tex' -printf "%p\n")
fi

#iterate over files
for FILE in $FILES
do
  #working file
  killSingle "$FILE"
done
