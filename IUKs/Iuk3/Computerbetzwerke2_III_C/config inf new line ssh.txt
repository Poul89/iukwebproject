enable 
config t 
line console 0 
logging synchronous

ssh:

! Namen für ins Zertifikat
hostname R1
ip domain-name ccna-lab.com

! Schlüsselpaar erzeugen
crypto key generate rsa modulus 2048

! Benutzeraccount anlegen
username admin secret Adm1nP@55

! Terminal für SSH konfigurieren
line vty 0 15
transport input ssh
login local